# Engine Control Room with Qt
Alarm system for various levels within an engine room. When the levels are at a certain low point an alarm sounds.

# Visual
![](control_room.PNG)

# Install
[Install Qt](https://www.qt.io/download)

# Resources
- [Free sound effects](https://mixkit.co/free-sound-effects/alarm/)
- [Documentation Qt 6.3](https://doc.qt.io/qt-6/index.html)
