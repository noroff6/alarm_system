import QtQuick
import QtMultimedia
import QtQuick.Controls
import QtQuick.Layouts

ApplicationWindow {
    width: 640
    height: 480
    visible: true
    font.family: "Courier New"

    title: qsTr("Control room")
    header: ToolBar {

            Text{
                anchors.horizontalCenter: parent.horizontalCenter
                text: "Engine control panel"
            }
        }

    property bool startEngine: false;
    property string source_sound: "";
    //property int bar_value: 1;

    MediaPlayer {
    id: soundplayer
    source: source_sound
    audioOutput : AudioOutput{}
    }

    Component{
        id: topText
        Text {
            text: headline
        }
    }

    Component{
        id: progress_bar

        ProgressBar {
            id: control
            property PropertyAnimation animation: animation
            width: 170
            height:50
            value: bar_value


            PropertyAnimation {
                id: animation
                    target: control
                    property: "value"
                    from: from_value
                    to: 0
                    duration: duration_value
                    running: startEngine
                    loops: Animation.Infinite
                }

            Text{
                text: level_type
                anchors.fill: parent
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter

            }

            Connections
            {
                target: control
                onValueChanged: function alarming()
                {
                    if(control.value <= 0.3)
                    {
                        source_sound = "qrc:/Mysounds/alarm.wav"
                        soundplayer.play()

                        if(control.value <=0.004){
                        animation.running = false
                        source_sound = "qrc:/Mysounds/mechanicalfault.wav"
                        soundplayer.play()
                        }
                    }
                }
            }
        }// Progressbar

    }

    Component{
        id: engine_button

        Button{
            width: 160
            height:50
            text: engine_action
            font.bold: true
            font.pixelSize: 14

            background: Rectangle {
                color: parent.hovered ? "white" : "yellow"
                radius: 50
            }


            onClicked: {
                if(text === "Start engine")
                {
                    startEngine = true
                    source_sound = "qrc:/Mysounds/startengine.wav";
                    soundplayer.play()
                }
                if(text === "Stop engine")
                {
                    startEngine = false
                }
                if(text === "Alarm engine room")
                {
                    source_sound = "qrc:/Mysounds/emergencyalarm.wav";
                    soundplayer.play()
                }

            }
        }

    }

        GridLayout{
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.horizontalCenterOffset: 0


            id: grid
            rows: 3
            columns: 3
            columnSpacing: 50
            rowSpacing: 50


            Loader {property string engine_action: "Start engine";
                    sourceComponent: engine_button}

            Loader {property string engine_action: "Stop engine"
                    sourceComponent: engine_button}

            Loader {property string engine_action: "Alarm engine room"
                    sourceComponent: engine_button}

            Loader {property int from_value: 1;
                property string level_type: "Oil level"
                property int duration_value: 10000;
                    sourceComponent: progress_bar;}

            Loader {property int from_value: 1;
                property string level_type: "Steam pressure"
                property int duration_value: 20000;
                    sourceComponent: progress_bar;}

            Loader {id: crude
                property int from_value: 1;
                property string level_type: "Crude oil level"
                property int duration_value: 40000;
                    sourceComponent: progress_bar;}

        RoundButton {
            text: "Stop engine room alarm"
            onClicked: soundplayer.stop()
        }

        RoundButton {
            text: "Fill oil"
            onClicked:
            {startEngine = false;
            startEngine = true;
            soundplayer.stop();}
        }

    }

        footer: TabBar {
           }

    }

